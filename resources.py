import sys
import os

def resources(relative_path):
    try:
        path = sys._MEIPASS
    except Exception:
        path = os.path.abspath(".")
        
    return os.path.join(path, relative_path)
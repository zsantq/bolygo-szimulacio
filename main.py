import pygame

from planet import Planet
from __colors__ import Colors
from resources import resources

pygame.init()

icon_url = resources("assets/icon.png")
icon = pygame.image.load(icon_url)
font = pygame.font.SysFont('arial', 14)

pygame.display.set_caption("Fizika Szimuláció")
pygame.display.set_icon(icon)

WIDTH, HEIGHT = 800, 800
WINDOW = pygame.display.set_mode((WIDTH, HEIGHT))

def main():
	clock = pygame.time.Clock()

	sun = Planet(Colors().SUN, 0, 0, 30, 1.98892 * 10**30)
	sun.sun = True
 
	mercury = Planet(Colors().MERCURY, 0.387 * Planet.AU, 0, 7.69, 3.30 * 10**23)
	mercury.y_velocity = -47.4 * 1000
 
	venus = Planet(Colors().VENUS, 0.723 * Planet.AU, 0, 18.18, 4.8685 * 10**24)
	venus.y_velocity = -35.02 * 1000
 
	earth = Planet(Colors().EARTH, 1 * Planet.AU, 0, 20, 5.9742 * 10**24)
	earth.y_velocity = 29.783 * 1000 

	mars = Planet(Colors().MARS, 1.524 * Planet.AU, 0, 10.52, 6.39 * 10**23)
	mars.y_velocity = 24.077 * 1000

	planets = [sun, mercury, venus, earth, mars]

	running = True
	while running:
		clock.tick(60)
		WINDOW.fill((0, 0, 0))

		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				running = False

		for planet in planets:
			planet.reposition(planets)
			planet.draw(WINDOW, font)

		pygame.display.update()

	pygame.quit()

main()
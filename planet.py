import pygame
import math

class Planet:
    TIMESTEP = 3600*24 # 1 nap
    
    # Valós adatok
    AU = 149.6e6 * 1000
    G = 6.67428e-11
    
    SCALE = 230 / AU  # 1AU = 100 pixel

    def __init__(self, color, x, y, radius, mass):
        self.color = color
        
        self.x = x
        self.y = y
        
        self.x_velocity = 0
        self.y_velocity = 0

        self.radius = radius
        self.mass = mass

        self.sun = False
        self.orbit = []

    def pull(self, planet):
        distance_x = planet.x - self.x
        distance_y = planet.y - self.y
        distance = math.sqrt(distance_x ** 2 + distance_y ** 2)


        # Egyszeru fizika
        # F = ma    

        force = self.G * self.mass * planet.mass / distance ** 2
        # Theta szög kiszámítása tangensel
        theta = math.atan2(distance_y, distance_x)
        force_x = math.cos(theta) * force
        force_y = math.sin(theta) * force
        
        return force_x, force_y

    def reposition(self, planets):
        total_x_force = total_y_force = 0
        
        for planet in planets:
            if self == planet:
                continue

            x_force, y_force = self.pull(planet)
            total_x_force += x_force
            total_y_force += y_force

        self.x_velocity += total_x_force / self.mass * self.TIMESTEP
        self.y_velocity += total_y_force / self.mass * self.TIMESTEP

        self.x += self.x_velocity * self.TIMESTEP
        self.y += self.y_velocity * self.TIMESTEP
        self.orbit.append((self.x, self.y))
        
    def draw(self, window, font):
        x = self.x * self.SCALE + window.get_width() / 2
        y = self.y * self.SCALE + window.get_height() / 2

        if len(self.orbit) > 2:
            updated_points = []
            for point in self.orbit:
                x, y = point
                x = x * self.SCALE + window.get_width() / 2
                y = y * self.SCALE + window.get_height() / 2
                updated_points.append((x, y))

            pygame.draw.lines(window, self.color, False, updated_points, 2)

        pygame.draw.circle(window, self.color, (x, y), self.radius)
        
        if self.color == (255, 255, 90, 1):
            text = font.render("Nap", 1, (255, 255, 255))
        elif self.color == (188,196,191, 0.9):
            text = font.render("Merkúr", 1, (255, 255, 255))
        elif self.color == (254,216,177, 0.9):
            text = font.render("Vénusz", 1, (255, 255, 255))
        elif self.color == (105, 129, 231, 0.9):
            text = font.render("Föld", 1, (255, 255, 255))
        elif self.color == (173,100,66, 0.9):
            text = font.render("Mars", 1, (255, 255, 255))
            
        window.blit(text, (x - text.get_width() / 2, y - text.get_height() / 2))